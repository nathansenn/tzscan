#!/bin/sh

opam pin -y --dev pgocaml
opam source pgocaml
opam remove pgocaml
(cd pgocaml.3.2;
  ./configure --enable-p4 --prefix $OPAM_SWITCH_PREFIX --docdir $OPAM_SWITCH_PREFIX/doc &&
  make &&
  make doc &&
  make install)
opam install pgocaml --fake
rm -rf pgocaml.3.2
