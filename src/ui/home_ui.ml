(************************************************************************)
(*                                TzScan                                *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  TzScan is distributed in the hope that it will be useful,           *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ocp_js
open Html
open Js_utils
open Bootstrap_helpers.Icon
open Bootstrap_helpers.Grid
open Bootstrap_helpers.Color
open Bootstrap_helpers.Panel
open Tezos_types
open Data_types
open Lang
open Text
open Common

(* cut float at 2 digits after dot *)
let simplify_float f =
  float_of_int (int_of_float (f *. 100.)) /. 100.

let home_transaction_id = "home-transactions"
let home_blocks_id = "home-heads"
let home_last_baker_id = "home-last-baker"
let home_last_baker_logo_id = "home-last-baker-logo"
let home_last_baker_website_id = "home-last-baker-website"
let home_circulating_supply_id = "home-circulating-supply"
let home_marketcap_id = "home-marketcap"
let home_volume_id = "home-volume"
let home_usd_price_id = "home-tezos-price-usd"
let home_btc_price_id = "home-tezos-price-btc"
let home_last_block_id = "home-last-block-container"
let home_sponsored_id = "home-sponsored-case"
let home_change_24h_id = "home-change-24h"

let period_container_id = "period-container"
let period_title_id = "period-title"
let period_info_id = "period-info"
let cycle_container_id = "cycle-container"
let blocks_loading_id = "home-blocks-loading"

let start_block_id = "start-block"
let end_block_id = "end-block"
let progress_title_id = "progress-title"
let bar_id = "bar"
let bar_span_id = "bar-span"

let odometer_active_baker_rate_id = "odometer-baker-rate"

let odometer_transac_id = "odometer-tr"
let odometer_orig_id = "odometer-or"
let odometer_del_id = "odometer-del"
let odometer_act_id = "odometer-act"

let fan_end_id = "fan-endors"
let fan_block_id = "fan-block"
let fan_baking_rate_id = "fan-baking-rate"
let fan_activation_rate_id = "fan-activation-rate"

let fab name = span ~a:[ a_class ["fab"; "fa-" ^ name ] ] []

let color ?(limit=0.) v =
  match float_of_string_opt v with
  | None -> []
  | Some f ->
    if f > limit then
      [ a_class [green]; a_title (Printf.sprintf "value above %.2g%%" limit) ]
    else if f < -1. *. limit then
      [ a_class [red]; a_title (Printf.sprintf "value below -%.2g%%" limit) ]
    else
      [ a_class [blue]; a_title (Printf.sprintf "value between -%.2g%% and %.2g%%" limit limit) ]

let additional_divs = ref
    (div [] : (Html_types.div_content_fun elt))

(* Last blocks *)
let make_home_blocks blocks =
  let theads = tr [
      th @@ cl_icon_xs clock_icon (t_ s_age);
      th @@ cl_icon_xs cube_icon (t_ s_level);
      th ~a:[ a_class [ "hidden-xs" ] ] @@
      cl_icon_xs cookie_icon (t_ s_baker);
      th ~a:[ a_class [ "hidden-xs" ] ] @@
      cl_icon_xs (number_icon cube_icon) (t_ s_nbops);
      th @@ cl_icon_xs Tez.icon (t_ s_volume);
      th ~a:[ a_class [ "hidden-xl" ] ] @@ cl_icon Tez.icon (t_ s_fees);
    ] in
  tablex ~a:[ a_class [ "table" ] ] [
    tbody @@
    theads ::
    List.map (fun block ->
        let timestamp_str = Date.to_string block.timestamp in
        let td_timestamp = td ~a:[ a_class [ "bk-home-ts" ] ] [ ] in
        Manip.appendChild td_timestamp
          (Format_date.auto_updating_timespan timestamp_str);
        tr [
          td_timestamp ;
          td [ make_link @@ string_of_int block.level ] ;
          account_w_blockies
            ~aclass:["hidden-xs"; "no-overflow"]
            ~crop_len:15
            block.baker ;
          td ~a:[ a_class [ "hidden-xs" ] ]
            [ txt @@ string_of_int block.nb_operations ] ;
          td [ Tez.pp_amount ~width:3 block.volume ] ;
          td ~a:[ a_class [ "hidden-xl" ] ] [
            Tez.pp_amount ~width:3 block.fees ] ;
        ]
      ) blocks
  ]

let update_progress level =
  let bar = find_component bar_id in
  let cycle_position = level.lvl_cycle_position in
  let cycle = level.lvl_cycle in
  let cst = Infos.constants ~cycle in
  let blocks_per_cycle = cst.blocks_per_cycle in
  let percent = (cycle_position + 1 )* 100 / blocks_per_cycle in
  Manip.SetCss.width bar (Printf.sprintf "%d%%" percent) ;
  let bar_span = find_component bar_span_id in
  let levels_left = cst.blocks_per_cycle - level.lvl_cycle_position in
  Manip.setInnerHtml bar_span
    (Printf.sprintf "%d%% Est. %s" percent
       (Format_date.time_before_level ~cst levels_left));
  let component = find_component progress_title_id in
  let to_update =
    p [
      cube_icon () ; space_icon () ;
      txt @@
      t_subst s_subst_block_cycle
        (function
            "level" -> string_of_int level.lvl_level
          | "cycle" -> string_of_int level.lvl_cycle
          | _ -> "????")
    ]  in
  Manip.replaceChildren component [ to_update ];
  let start_block = find_component start_block_id in
  let end_block = find_component end_block_id in
  Manip.setInnerHtml start_block @@
  string_of_int (level.lvl_cycle * blocks_per_cycle + 1);
  Manip.setInnerHtml end_block @@
  string_of_int ((level.lvl_cycle + 1) * blocks_per_cycle);
  ()

let update_blocks blocks =
  let block_table = make_home_blocks blocks in
  let content_div = find_component home_blocks_id in
  begin try
      let loading_div = find_component blocks_loading_id in
      Manip.removeSelf loading_div;
    with _ -> ()
  end ;
  Manip.removeChildren content_div;
  Manip.appendChild content_div block_table

let get_sponsored : (Data_types.service list -> Data_types.service list) ref =
  ref (fun _ -> [])

let get_ads : (Data_types.service list -> Data_types.service list) ref =
  ref (fun _ -> [])

let update_baker services baker =
  let container = find_component home_last_baker_id in
  let sponsors = !get_sponsored services in
  let website_container = find_component home_last_baker_website_id in
  let logo_container = find_component home_last_baker_logo_id in
  let sponsor =
    List.find_opt (fun b ->
        match b.srv_tz1 with
        | None -> false
        | Some tz1 -> tz1 = baker.tz)
      sponsors in
  begin
    match sponsor with
    | None ->
      let new_logo =
        Base58Blockies.create ~scale:16 baker.tz in
      Js_utils.hide website_container ;
      (* Manip.replaceChildren
       *   website_container [ a [ fas "cookie-bite" ]]; *)
      Manip.replaceChildren logo_container [ new_logo ]
    | Some sponsor ->
      Js_utils.show website_container ;
      Manip.replaceChildren
        website_container [ a ~a:[ a_href sponsor.srv_url] [ fas "link" ]];
      let new_logo =
        a ~a:[ a_href sponsor.srv_url ]
          [ img ~src:(Common.img_path sponsor.srv_logo) ~alt:sponsor.srv_name () ] in
      Manip.replaceChildren logo_container [ new_logo ]
  end ;
  Manip.replaceChildren container [ make_link_account ~crop_len:30 baker ]

let update_ads services =
  let container = find_component home_sponsored_id in
  let ads =
    List.filter (fun s -> match s.srv_sponsored with
        | Some tsp when Common.time_diff tsp > 0. -> true
        | _ -> false ) (!get_ads services) in
  let ad =
    match ads with
    | [] ->
      {
        srv_kind = "ads" ;
        srv_tz1 = None ;
        srv_name = "Sponsored" ;
        srv_url = "" ;
        srv_logo = "tzscan-icon.png" ;
        srv_logo2 = None ;
        srv_logo_payout = None ;
        srv_descr = Some "You want to be listed here ? Slots available !" ;
        srv_sponsored = None ;
        srv_page = None ;
        srv_delegations_page = false ;
        srv_account_page  = false ;
        srv_aliases = None ;
        srv_display_delegation_page  = false;
        srv_display_account_page = false;
      }
    | ads -> let minutes = (jsnew Js.date_now ())##getMinutes() in
      let hours = (jsnew Js.date_now ())##getHours() in
      let winner = (minutes + ( 60 * hours )) mod (List.length ads + 1 ) in
        try
          List.nth (List.rev ads) winner
        with _ -> assert false
  in

  let new_content =
    match ad.srv_descr with
    | None ->
      div [
        a ~a:[ a_href ad.srv_url ]
          [ img ~src:(Common.img_path ad.srv_logo) ~alt:ad.srv_name () ]
      ]
    | Some descr ->
      div [
        span ~a:[ a_class [ clg4 ; csm3; cxs2 ;
                            "hidden-xxs" ] ] [
          a ~a:[ a_href ad.srv_url ]
            [ img ~src:(Common.img_path ad.srv_logo) ~alt:ad.srv_name () ]
        ] ;
        span ~a:[ a_class [ clg6 ; csm9; cxs10; "home-sponsored-descr" ] ]  [
          txt descr
        ]
      ] in
  Manip.replaceChildren container [ new_content ] ;
  Manip.replaceChildren (find_component "home-sponsored-title")
    [ a ~a:[ a_href ad.srv_url ] [ txt "Sponsored" ] ]

let update_leftbox_marketcap price_usd price_btc volume
    change_1 change_24 _change_7 _last_updated supply_info =
  let unopt ~default = function None -> default | Some v -> v in
  let price_usd = float_of_string price_usd in
  let volume = unopt ~default:"- " volume in
  let change_1 = unopt ~default:"- " change_1 in
  let change_24 = unopt ~default:"- " change_24 in
  let current_marketcap =
    Int64.(of_float @@
           ((to_float supply_info.current_circulating_supply) *. price_usd)) in
  let circulating_supply_container = find_component home_circulating_supply_id in
  let marketcap_container = find_component home_marketcap_id in
  let volume_container = find_component home_volume_id in
  let usd_price_container = find_component home_usd_price_id in
  let btc_price_container = find_component home_btc_price_id in
  (* let change_24_container = find_component home_change_24h_id in *)
  let up_icon () = fas "arrow-up" in
  Manip.replaceChildren circulating_supply_container [
    h3 [ Tez.pp_amount ~width:5 ~precision:2 supply_info.current_circulating_supply ]
  ] ;
  Manip.replaceChildren usd_price_container [
    h3 [
      span [
        txt @@ Printf.sprintf "%.2f" price_usd ;
        space () ;
        Tez.dollar () ;
        space () ;
        span ~a:(a_style "font-size:13px" ::
                 color ~limit:0.25 change_1 ) [
          begin if float_of_string change_1 < 0. then
              down_icon ()
            else
              up_icon ()
          end ;
          space () ;
          txt @@ change_1 ^ "%"
        ]
      ]
    ]
  ] ;
  Manip.replaceChildren btc_price_container [
    h3 [
      span ~a:(color ~limit:0.25 change_1) [
        txt @@ price_btc ^ " BTC"
      ]
    ]
  ] ;
  Manip.replaceChildren marketcap_container [
    h3 [ Tez.pp_amount ~width:3 ~precision:2 ~icon:Tez.dollar current_marketcap ]
  ] ;

  Manip.replaceChildren volume_container [
    h3 [
      span [
        Tez.pp_amount ~width:4 ~precision:2 ~icon:Tez.dollar
          (Int64.of_float @@ float_of_string volume *. 100_000.) ] ;
      space () ;
      span ~a:(a_style "font-size:13px"  :: color ~limit:0.25 change_24 ) [
        begin if float_of_string change_24 < 0. then
            down_icon ()
          else
            up_icon ()
        end ;
        space () ;
        txt @@ change_24 ^ "%"
      ]
    ]
  ]

let update_fan h24 =
  let end_rate = int_of_float h24.h24_end_rate in
  let blocks_rate = int_of_float h24.h24_block_0_rate in
  let baking_rate = int_of_float h24.h24_baking_rate in
  let exec_end =
    Printf.sprintf "var bar = document.getElementById('%s').ldBar; bar.set(%d)"
      fan_end_id
      end_rate in
  let exec_blocks =
    Printf.sprintf "var bar = document.getElementById('%s').ldBar; bar.set(%d)"
      fan_block_id
      blocks_rate in
  let exec_baking_rate =
    Printf.sprintf "var bar = document.getElementById('%s').ldBar; bar.set(%d)"
      fan_baking_rate_id
      baking_rate in
  ignore (Js.Unsafe.eval_string exec_end) ;
  ignore (Js.Unsafe.eval_string exec_blocks) ;
  ignore (Js.Unsafe.eval_string exec_baking_rate)

let init_fan () =
  let fan_end = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fan_end_id in
  let fan_bl = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fan_block_id in
  let fan_br = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fan_baking_rate_id in
  ignore (Js.Unsafe.eval_string fan_end) ;
  ignore (Js.Unsafe.eval_string fan_bl) ;
  ignore (Js.Unsafe.eval_string fan_br)

let make_fan percent fan_init_class =
  let set_fan bar =
    let barjs = To_dom.of_element bar in
    barjs##setAttribute(Js.string "data-value",
                        Js.string @@ string_of_int percent) ;
    barjs##setAttribute(Js.string "data-preset",
                        Js.string "circle") in
  let pb =
    div ~a:[ a_id fan_init_class ;
             a_class [ fan_init_class; "ldBar"; "label-center"; "auto" ] ] [] in
  ignore (set_fan pb);
  pb

let make_sm_fan ?color percent fan_init_class =
  let set_fan bar =
    let barjs = To_dom.of_element bar in
    barjs##setAttribute(Js.string "data-value",
                        Js.string @@ string_of_int percent) ;
    barjs##setAttribute(Js.string "style",
                        Js.string "width:50%; height:50%");
    begin match color with
      | Some color ->
        barjs##setAttribute(Js.string "data-stroke",
                            Js.string color);
      | None -> () end ;
    barjs##setAttribute(Js.string "data-preset",
                        Js.string "circle") in
  let pb =
    div ~a:[ a_id fan_init_class ;
             a_class [ fan_init_class; "ldBar"; "label-center"; "auto" ] ] [] in
  ignore (set_fan pb);
  pb

let update_odometer h24 =
  let od1 = find_component odometer_transac_id in
  let od2 = find_component odometer_orig_id in
  let od3 = find_component odometer_del_id in
  let od4 = find_component odometer_act_id in
  let od5 = find_component odometer_active_baker_rate_id in
  Manip.setInnerHtml od1 @@ string_of_int h24.h24_transactions ;
  Manip.setInnerHtml od2 @@ string_of_int h24.h24_originations ;
  Manip.setInnerHtml od3 @@ string_of_int h24.h24_delegations ;
  Manip.setInnerHtml od4 @@ string_of_int h24.h24_activations ;
  Manip.setInnerHtml od5 @@ string_of_int h24.h24_active_baker

let init_odometer () =
  let od1 = To_dom.of_element @@ find_component odometer_transac_id in
  let od2 = To_dom.of_element @@ find_component odometer_orig_id in
  let od3 = To_dom.of_element @@ find_component odometer_del_id in
  let od4 = To_dom.of_element @@ find_component odometer_act_id in
  let od5 = To_dom.of_element @@ find_component odometer_active_baker_rate_id in
  ignore @@ Odometer.odometer od1 ;
  ignore @@ Odometer.odometer od2 ;
  ignore @@ Odometer.odometer od3 ;
  ignore @@ Odometer.odometer od4 ;
  ignore @@ Odometer.odometer od5

let update_h24 h24 =
  update_odometer h24 ;
  update_fan h24

let color_pc value threshold =
  if value >= threshold then green
  else red

let make_quorum_panel turn_out expected_quorum smajority =
  let period_quorum_fan_id = "period-quorum-fan-id" in
  let period_majo_fan_id = "period-majo-fan-id" in
  let to_color = color_pc turn_out expected_quorum in
  let turn_out = int_of_float turn_out in
  let sm_color = color_pc smajority 80. in
  let majo = int_of_float smajority in
  let fan_quorum =
    div ~a:[ a_class [ "period-fan"; to_color ] ] [
      make_sm_fan turn_out ~color:to_color period_quorum_fan_id ] in
  let fan_majo =
    div ~a:[ a_class [ "period-fan"; sm_color ] ] [
      make_sm_fan majo ~color:sm_color period_majo_fan_id ] in
  let quorum =
    div [
      fan_quorum ;
      div ~a:[ a_class [ "sub-sm-fan" ] ] [
        span [ txt @@ Printf.sprintf "Threshold %.1f%%" expected_quorum ] ] ;
    ] in
  let majo =
    div [
      fan_majo ;
      div ~a:[ a_class [ "sub-sm-fan" ] ] [
        span [ txt @@ Printf.sprintf "Threshold %.0f%%" 80. ] ] ;
    ] in
  [ period_quorum_fan_id ; period_majo_fan_id ],
  div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ quorum ],
  div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ majo ]

let make_period_sum_div sum =
  let period_progress_fan_id = "period-p-fan-id" in
  let cst = Infos.constants ~cycle:sum.sum_cycle in
  let start_level = sum.sum_period * cst.blocks_per_voting_period + 1 in
  let end_level = (sum.sum_period + 1) * cst.blocks_per_voting_period in
  let period_position = sum.sum_level - start_level + 1 in
  let est_time =
    if end_level - sum.sum_level = 0 then ""
    else
      "~ " ^
      (Format_date.time_before_level ~cst (end_level - sum.sum_level)) ^
      " left" in
  let percent = period_position * 100 / cst.blocks_per_voting_period in
  let fan_progress =
    div ~a:[ a_class [ "period-fan" ] ] [
      make_sm_fan percent period_progress_fan_id ] in
  let progress =
    div ~a:[ a_class [ cxs12; csm4 ] ] [
      fan_progress ;
      div ~a:[ a_class [ "sub-sm-fan" ] ] [
        span [ txt @@ Printf.sprintf "%s" est_time ] ] ;
    ] in
  let fans, div_info1, div_info2 =
    match sum.sum_period_info with
    | Sum_proposal_empty ->
      [],
      div ~a:[ a_class [ "hidden-xs" ; cxs8 ; csm8 ] ] [ span [ txt "No proposal yet" ] ],
      div []
    | Sum_proposal (hash, count, pc) ->
      let number_div =
        div [
          div [
            span ~a:[ a_class [ "period-sum-prop-number" ] ] [
              txt @@ string_of_int count ] ] ;
          div [
            span ~a:[ a_class [ "sub-sm-fan" ] ] [ txt "proposals" ] ]
        ]
      in
      let link = match List.assoc_opt hash !Proposals_ui.archive_links with
        | Some (name, path) -> Common.make_link ~crop_len:20 ~path name
        | None -> Common.txt_ () in
      let pc_div =
        div [
          div [
            span ~a:[ a_class [ "period-sum-prop-number"; green ] ] [
              txt @@ Printf.sprintf "%.0f%%" pc ]
          ] ;
          div [
            span ~a:[ a_class [ "sub-sm-fan" ] ] [ link ] ]
        ]
      in
      [],
      div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ number_div],
      div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ pc_div ]
    | Sum_testing_vote (turn_out, expected_quorum, smajority) ->
      make_quorum_panel turn_out expected_quorum smajority
    | Sum_testing ->
      [],
      div ~a:[ a_class [ "hidden-xs" ; cxs8 ; csm8 ] ] [ span [ txt "Currently in testing" ] ],
      div []
    | Sum_promo (turn_out, expected_quorum, smajority) ->
      make_quorum_panel turn_out expected_quorum smajority
  in
  period_progress_fan_id :: fans, div [ progress ; div_info1 ; div_info2 ]

let update_current_period period_sum =
  let container = find_component period_container_id in
  let title_div = find_component period_title_id in
  let str = Utils.voting_period_of_summary period_sum.sum_period_info in
  let p_title_str = Printf.sprintf "Period %d : %s" period_sum.sum_period str in
  let xs_p_title_str = Printf.sprintf "%d : %s" period_sum.sum_period str in
  let new_title = [
    span ~a:[ a_class [ "hidden-xs" ] ] [
      fas "vote-yea" ; space_icon () ;
      a ~a:(a_link "/proposals") [ txt p_title_str ] ];
    span ~a:[ a_class [ "visible-xs" ] ] [
      fas "vote-yea" ; space_icon () ;
      a ~a:(a_link "/proposals") [ txt xs_p_title_str ] ];
  ]
  in
  let fans, to_update = make_period_sum_div period_sum in
  Manip.removeChildren container ;
  Manip.removeChildren title_div ;
  List.iter (Manip.appendChild title_div) new_title ;
  Manip.appendChild container to_update ;
  List.iter (fun fid ->
      let fan = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fid in
      ignore (Js.Unsafe.eval_string fan)
    ) fans

let make_stats () =
  let marketcap_title =
    h2 [
      chart_line_icon () ;
      space_icon () ;
      txt_t s_market_cap ;
    ] in
  let total_circulating_title =
    h2 [
      span ~a:[ a_class [ "hidden-xs" ] ] [
        tz_icon () ; space_icon () ;
        txt_t s_circulating_supply ];
      span ~a:[ a_class [ "visible-xs" ] ] [
        tz_icon () ; space_icon () ;
        txt_t s_circulating_supply_xs ];
    ] in
  let volume_title =
    h2 [
      fas "dollar-sign";
      space_icon () ;
      txt "Volume 24h"
    ] in
  let usd_price_title =
    h2 ~a:[ a_title "Tezos USD Price (last hour)" ] [
      fas "dollar-sign";
      space_icon () ;
      txt "Tezos USD Price"
    ] in
  let btc_price_title =
    h2 [
      fab "btc" ;
      space_icon () ;
      txt "Tezos BTC Price"
    ] in
  let progress_title =
    div ~a:[ a_id progress_title_id ] [
      span [
        cube_icon () ;
        space_icon () ;
        txt_t s_block
      ]
    ] in
  let period_title =
    h2 ~a:[ a_id period_title_id ] [
      span ~a:[ a_class [ "hidden-xs" ] ] [
        fas "vote-yea" ; space_icon () ;
        txt_t s_tzscan_period ];
      span ~a:[ a_class [ "visible-xs" ] ] [
        fas "vote-yea" ; space_icon () ;
        txt_t s_tzscan_period_xs ];
    ] in
  let last_baker_title =
    h2 [
      cookie_icon () ;
      space_icon () ;
      txt "Latest Baker"
    ] in

  let sponsored_title =
    h2 [
      fas "star" ;
      space_icon () ;
      span ~a:[ a_id "home-sponsored-title" ] [ txt "Sponsored" ]
    ] in
  let period_div =
    div ~a:[ a_id period_container_id ] [] in
  let start_block = div ~a:[a_class [clg3 ; cxs3]; a_id start_block_id] [] in
  let cycle_container =
    div ~a:[ a_class ([clg6; cxs6] @ [ "no-overflow" ]); a_id cycle_container_id ] [
      div ~a:[ a_class [ "progress" ] ] [
        div ~a:[ a_id bar_id; a_class ["progress-bar"; "progress-bar-striped"];
                 a_role [ "progressbar" ] ] [
          span ~a:[ a_id bar_span_id ] []]]] in
  let end_block = div ~a:[a_class [clg2 ; cxs3]; a_id end_block_id ] [] in

  (* Panels *)
  div ~a:[ a_class [ row ] ] [
    div ~a:[ a_class [ clg1 ; csm1; "hidden-xs"] ] [ ] ;
    div ~a:[ a_class [ clg2 ; csm5; cxs6] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(total_circulating_title)
        ~panel_body_content:[
          div ~a:[ a_id home_circulating_supply_id ] [ txt "0" ] ] ()
    ] ;
    div ~a:[ a_class [ clg2 ; csm5 ; cxs6 ] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(marketcap_title)
        ~panel_body_content:[ div ~a:[ a_id home_marketcap_id] [ txt "0" ] ] ()
    ] ;
    div ~a:[ a_class [ clg2 ; csm5; cxs6 ; clgoffset0 ; csmoffset1 ; cxsoffset0 ] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(usd_price_title)
        ~panel_body_content:[ div ~a:[ a_id home_usd_price_id ] [ txt "0" ]] ()
    ] ;
    div ~a:[ a_class [ clg2 ; csm5 ; cxs6 ] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(btc_price_title)
        ~panel_body_content:[
          div ~a:[ a_id home_btc_price_id ] [ txt "0" ] ] ()
    ] ;
    div ~a:[ a_class [ clg2 ; "hidden-md"; "hidden-sm" ; "hidden-xs"] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(volume_title)
        ~panel_body_content:[ div ~a:[ a_id home_volume_id ] [ txt "0" ] ] ()
    ] ;
    div ~a:[ a_class [ clg1 ; "hidden-xs"] ] [ ] ;

    (* new row *)
    div ~a:[ a_class [ clg3 ; csm6; cxs12] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(
          div [ progress_title ])
        ~panel_body_content:[
          div ~a:[ a_class ["no-overflow" ]; a_id home_last_block_id ]
            [ start_block ; cycle_container ; end_block ] ] ()
    ] ;
    div ~a:[ a_class [ clg3 ; csm6; cxs6] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(last_baker_title)
        ~panel_body_content:[
          div [
            div ~a:[ a_class [ clg2; csm2; "hidden-xs" ;
                               clgoffset1 ; csmoffset1 ; cxsoffset1  ] ;
                     a_id home_last_baker_logo_id ] [ ] ;
            div ~a:[ a_class [ clg7 ; csm7 ; cxs9 ; "no-overflow";
                               clgoffset0 ; csmoffset0 ; cxsoffset1 ] ;
                     a_id home_last_baker_id ]  [ txt "--" ] ;
            div ~a:[ a_class [ clg1; csm1; cxs1; "hidden-sm"; "hidden-xs" ] ;
                     a_id home_last_baker_website_id ]  [
              space () ] ;
          ]
        ] ()
    ] ;
    div ~a:[ a_class [ clg3 ; csm6; cxs6] ] [
      make_panel
        ~panel_class:[ "home-panel" ]
        ~panel_title_content:(period_title)
        ~panel_body_content:[ period_div ] ()
    ] ;
    div ~a:[ a_class [ clg3 ; csm6; cxs12] ] [
      make_panel
        ~panel_class:[ "home-panel" ; "home-sponsored" ]
        ~panel_title_content:(sponsored_title)
        ~panel_body_content:[ div ~a:[ a_id home_sponsored_id ] [ txt "--" ] ] ()
    ]
  ]

let make_page () =
  (* Stats *)
  let stats_div =
    div [ make_stats () ] in

  (* Blocks  *)
  let block_div =
    div ~a:[ a_id home_blocks_id ;
             a_class [ "blocks-div"; csm12 ] ] [] in

  (* Latests Blocks/Transactions *)
  div ~a:[ a_class [ row; "summary" ] ] [
    stats_div ;
    div ~a:[ a_class [ clg6; cxs12 ] ] [
      div ~a:[ a_class [ row ] ] [
        div ~a:[ a_class [ clg11; "section-title" ] ]
          [ cubes_icon () ; space_icon () ;
            txt_t s_blocks ; Glossary_doc.(help HBlock);
            span [a ~a:(a_link ~aclass:[ "paginate"; "ntm" ] "/blocks")
                    [ txt_t s_view_all ]
                 ]
          ] ;
        make_home_loading_gif blocks_loading_id [ cxs12 ];
        block_div;
      ]
    ] ;
    div ~a:[ a_class [ clg6; cxs12 ] ] [
      div ~a:[ a_class [ row ] ] [

        div ~a:[ a_class [ cxs12; "section-title" ] ]
          [ chart_line_icon () ; space_icon () ;
            txt_t s_last_24h ;
          ] ;

        div ~a:[ a_class [ cxs6; "stat-item" ] ] [
          div [ h4 [ txt_t s_endorsement_rate ] ] ;
          make_fan 0 fan_end_id ] ;
        div ~a:[ a_class [ cxs6; "stat-item" ] ] [
          div [ h4 [ txt_t s_block_prio_0_baked ] ] ;
          make_fan 0 fan_block_id ] ;

        div ~a:[ a_class [ cmd3 ; cxs6; "stat-item"; "odometer-item" ] ] [
          a ~a:(a_link "transactions") [
            div [ h4 [ txt_t s_transactions] ] ;
            div ~a:[ a_id odometer_transac_id ;
                     a_class [ "odometer"; "odometer-theme-train-station" ] ] [
              txt "0"] ;
          ]
        ] ;
        div ~a:[ a_class [ cmd3 ; cxs6; "stat-item"; "odometer-item" ] ] [
          a ~a:(a_link "originations") [
            div [ h4 [ txt_t s_originations] ] ;
            div ~a:[ a_id odometer_orig_id ;
                     a_class [ "odometer"; "odometer-theme-train-station" ] ] [
              txt "0"] ;
          ]
        ] ;
        div ~a:[ a_class [ cmd3; cxs6; "stat-item"; "odometer-item" ] ] [
          a ~a:(a_link "delegations") [
            div [ h4 [ txt_t s_delegations ] ] ;
            div ~a:[ a_id odometer_del_id ;
                     a_class [ "odometer"; "odometer-theme-train-station" ] ] [
              txt "0"] ;
          ]
        ] ;
        div ~a:[ a_class [ cmd3; cxs6 ; "stat-item"; "odometer-item" ] ] [
          a ~a:(a_link "activations") [
            div [ h4 [ txt_t s_activations ] ] ;
            div ~a:[ a_id odometer_act_id ;
                     a_class [ "odometer"; "odometer-theme-train-station" ] ] [
              txt "0"] ;
          ]
        ] ;
        div ~a:[ a_class [ cxs12; "section-title" ] ]
          [ chart_line_icon () ; space_icon () ;
            txt_t s_last_snapshot ] ;

        div ~a:[ a_class [ cxs6; "stat-item" ] ] [
          div [ h4 [ txt_t s_staking_ratio ] ] ;
          make_fan 0 fan_baking_rate_id
        ] ;

        div ~a:[ a_class [ cxs6; "stat-item" ] ] [
          a ~a:(a_link "rolls-distribution") [
            div [ h4 [ txt_t s_roll_owners ] ] ;
            div ~a:[ a_id odometer_active_baker_rate_id ;
                     a_class [ "odometer"; "odometer-theme-train-station" ] ] [
              txt "0"] ;
          ]
        ] ;

      ]
    ]
  ]
