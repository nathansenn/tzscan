#!/bin/sh

git clone https://github.com/inhabitedtype/httpaf.git
(cd httpaf;
 git checkout 025f5bc94a48a7aa1c2dee4fd6e5667c0268d2a7
 opam pin add httpaf .
 opam pin add httpaf-lwt-unix .)
rm -rf httpaf
